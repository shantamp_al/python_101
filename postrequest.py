import json
import requests

# What if we want 3 post codes?
## You can use a POST request
### Post request have url + path + argument AND can take a JSON object
#### (All request also have headers AKA metadata)

base_url = "https://api.postcodes.io/postcodes" ## This is a variable

# data to be sent to api
# below is a variable
json_post_codes = {
    "postcodes" : ["OX49 5NU", "M32 0JG", "NE30 1DP"]
}

# use the Request library to make a post request
requests_response = requests.post(url=base_url, data=json_post_codes)
print(requests_response)
# print(requests_response.content)

parsed_content = json.loads(requests_response.text)
print(parsed_content)
print(type(parsed_content))


# sending post request and saving response as a response 

# requests_response_dict = request_response.json()

# print(requests_response_dict)

# Get out the 3 lats and longs
## 
