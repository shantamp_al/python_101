# Functions
# Functions are like a machine, they can take in arguments, do some work and output a value.
# To work, functions must be called.

# Concepts:
# DRY - Don't Repeat Yourself
    # Avoid repetition in your code - functions can help
    # Maintainable

# Good functions:
    # Are like people - They should have one job!
    # Makes it easier to manage
    # Makes it easier to measure 
    # And test
    # Important - Don't print inside a function.

# Syntax 
# def <function_name(arg1, arg2, arg*)>:
    # Block of code 
    # Block of code
    # return <value>

def say_hello():
     return "hellooo"

# calling functions
say_hello()
print(say_hello())

# Function with arf 
def say_hello_human(human):
    return "hellooo " + human

print(say_hello_human('Shannon'))

def full_name_user_ask():
    f_name = input('>provide a first name: \n >').strip().upper()
    l_name = input('> proivde a last name: \n >').strip().upper()

    return f_name + ' ' +l_name

##### ^^^^ The above will print out the input in capitals.

# You can have defaults

def full_name(f_name="Alan", l_name="Turin"):
    return f_name + ' ' + l_name

print(full_name())
print(full_name('Shannon'))
print(full_name(l_name='Tamplin'))
