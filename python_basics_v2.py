# Strings
# A list of unorganised characters that can have spaces, numbers and any characters.
# syntax "" or ''
print('this is a string')
print(type('this too is a string'))

my_string = "this is a nice simple string"

# len() -> length of string
print(len(my_string))

# Useful string methods
# object.capitalise()

print(my_string.capitalize())

# .lower() --> all characters lower case

print(my_string.lower())

# .upper() --> all characters upper case

print(my_string.upper())

# .title()
print(my_string.title())

# Concactenation of strings
## The joining of two strings

name = "shannon"
greeting = 'Howdy'

print(greeting + name)
print(greeting, name)
print(greeting + ' ' + name)


# Interpolation with F strings
# You can interpolate into a string a value
# syntax print(f")

print("Welcome and howdy <name>")
print(f"Welcome and howdy {name}!")
print(f"Welcome and howdy {name.upper()}")

# String.. as said in the start, are lists of characters BUT are a list.
print(name[0])

# Numerical types
# These are numbers. we have integers, floats and a few others that are less used.
# You can perform mathemathical arithmatics with them.

my_number = 16 
print(my_number)
print(type(my_number))

num_a = 8
num_b = 24

# Add, subtract, divide, subtract, multiply and others
print(num_a + num_b)
print(num_a - num_b)
print(num_b * 12345)

# All of the above are intergers.
# Just a whole number without .0202
print(num_b/2)
print(type(num_b/2))

# Floats are composite numbers, everytime you divide you always get a float.
print(type(3.14))

## There are comparison operators that you can use logically with maths.
## such as greater than, smaller than and so on.
# The result is what is called a boolean

# A boolean is a data type that is either True or False

# Booleans - True or False
print(type(True)) # <class 'bool'>
print(type(False)) # <class 'bool'>

## Comparison Operators
num_a = 8 
num_b = 24

# Greater than
print(num_a > num_b) # False
print(num_b > num_a) # True
print(type(num_a > num_b))

# Smaller than 
print(num_a < num_b) # True
print(num_b < num_a) # False

# greater than or equal
print(num_a >= 8)
print(num_a >= num_b)

# Smaller than or equal
print(num_a <= 8)
print(num_a <= num_b)

# Equal to
print(num_a == num_b)
# one = is assignment, hence most languages use more than one equals.

# Not equal
print(num_a != num_b)

## Comparison operator also work for strings
print('is the string 10 equal to integer 10 in py?', '10' == 10)

example_input = 10 
user_input=input("> provide input:  ")
print(example_input == user_input)
print(type(example_input))
print(type(user_input))

# Casting
## User input is captured as a string
## helps us change data type where possible
## very useful when taking in user input.

## Change a integer to string 
# str() --> str 

example_int = 101
example_srt = str(example_int)
print(type(example_srt))

# ## change str into an integer 
# # Will only work with clean strings using numerical types only 
print(type(int('10')))







