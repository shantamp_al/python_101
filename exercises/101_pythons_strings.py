# Define the following variables
# name, last_name, age, eye_color, hair_color, good_person=

# first_name = input("Enter your first name ")
# last_name = input("Enter your last name")
# eye_colour = input("Enter your eye colour")
# hair_colour = input("Enter your hair colour")
# age = input("How old are you?")
# good_person = input("are you a good or bad person")

#print("Hi", first_name )
# Ask/Prompt user for input and Re-assign the above variables

# Print them back to the user as conversation
# Example: 'Hi there Jack! Your age is 36, you have green eye, black hair and your are a good person!"

# print(f'Hi there, {first_name} {last_name}, your have {eye_colour} eyes, {hair_colour} hair. You are also {age} years old and you are a {good_person} ')

#Extra Question 2 - Year of birth calculation? and responde back.
# print something like: 'You told the machine you are 28 hence you must have been born 1991!.. give or take'

import datetime

age = int(input("Enter your age"))
year = (datetime.datetime.today().year)-age

print(f'You told the machine you are {age} hence you must have been born in {year}!... give or take ')

# Extra Question 3 - Cast your input so age is stored as numerical type

print(type(age))
age = int
print(type(age))