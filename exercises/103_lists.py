# Lists - LET's use some lists!
# Define a list with amazing things inside!
    # For example, best places to eat or things you would do if you won the lottery? 
    # It must have 5 items
    # Complete the sentence:
    # Lists are organized using: _______????? 



## Define a list with 5 amazing things inside!

favourite_restaurants = [ 'Wagamamas', 'Nandos', 'Mcdonalds', 'Five Guys', 'Weatherspoons']

# Print the lists and check the data type of the list?
print(f'My top 5 restaurants are .. {favourite_restaurants}')
print(type(favourite_restaurants))

# Print the list's first object

print(favourite_restaurants[0])

# Print the list's second object

print(favourite_restaurants[1])

# Print the list's last object

print(favourite_restaurants[4])

# Check the type of the list's first and second object?
# is it the same as the list it self?

print(favourite_restaurants[0])


# Re-define the second item on the list


# Re-define another item on the list and print all the list


# Add an item to the list then print the list


# Remove an item from the list then print the list



# Add two items to list then print the list



