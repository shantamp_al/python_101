import json
import sys
import requests 

Postcode =(input('Please provide your postcode').strip())

response = requests.get(f'https://api.postcodes.io/postcodes/{Postcode}')

parsed_content = json.loads(response.text)
# print(parsed_content)

print(f"I have found the latitude = { parsed_content ['result']['latitude'] }")

