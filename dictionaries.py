## Dictonaries in Python
# Allow us to keep complex data.
    # Mike + his number and address
    # Anna 
# It's very simple, it works like a real dictonary.
# It jumps to the word you are looking for and has a value for it.

# Syntax
# {}
# {"key" : value}
print(type({})) # > <class 'dict'>

#Example

example_dict = {
    "human1" : "Mike",
    "human2" : "Anna"
}

# print the dictonary 
print(example_dict)

# use the keys to get values - like a list but with keys.

print(example_dict["human2"])

# Reassigning values
example_dict["human2"] = "Arnold"
print(example_dict)

# Make key value pairs on the fly.
# example_dict["human47"]

###
person_1 = {
    "name" : "Britney",
    "Age"  : 30,
    "Skills" : "Media"
}

person_2 = {
    "name" : "Justin Timberlake",
    "Age"  : 34,
    "Skills" : "Other"
}

list_of_dictionaries = [person_1, person_2]

print(list_of_dictionaries)

## If you just want .keys() or the .values()
print(person_1.keys)
print(person_1.values)
