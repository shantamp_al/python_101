## Print command
## print command maks code output to the terminal

print( 'hello world!') 

## ^^this does not mean that code cannot run without outputting to the terminal 
10*100
20*33
print(20*2) # the only one that get printed

## type()
# helps you identify the type of data, (specifically what class) For example:
print(10)
print(type(10))

print('10')
print(type('10'))

## Variables
# A variable is like a box, it has a name, we can put stuff inside and get the stuff that is inside.
# We can also at any point put other stuff inside the same box
# We could have a box called books, and put "Rich dad, Poor dad"

# Assignment of variable books to a string
books = "Rich dad, Poor dad"

# Calling of the variable

print(books)
print(type(books))

## prompting user for input
# syntax input ('messge')

input('Tell me a number')

# You need to capture the input into a variable to use later

user_var = input('Tell me a number again')
print(user_var)

# Python Errors

# Syntax error
# No name error

