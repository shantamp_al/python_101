import json
import sys
import requests

response = requests.get('https://www.boredapi.com/api/activity')

if response.status_code != 200:
    print('Something went wrong')
    sys.exit()
else:
    print("Got status 200 from the API!")


parsed_content = json.loads(response.text)

print("I read the following information")
print(parsed_content['activity'])
print(response.status_code)

