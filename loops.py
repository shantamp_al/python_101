# # For loops

# # Syntax
# # for x in <iterable>:
#     #block of code

# farhiya_gifts = ['Roller Skates', 'Birthday Cake', 'Vacation']

# for item_placeholder in farhiya_gifts:
#     print(item_placeholder)

# ## Embedded lists

# farhiya_gifts_2 = [['Roller Skates', 'Birthday Cake', 'Vacation'], ['stuffed toys' 'gameboy' 'bike']]

# for list_item in farhiya_gifts_2:
#     print(list_item)
#     for item in list_item:
#         print(item)

# ## Lets say we want to create a string with all the items
# farhiya_gifts_2 = [['Roller Skates', 'Birthday Cake', 'Vacation'], ['stuffed toys' 'gameboy' 'bike']]

# for list_list in farhiya_gifts_2:
#     print(list_list)

# # Let's first iterate over the list
# # Then get each individual item and add it to  string
# # Let's start a new variable to hold said string

# result_s = ''

# for list_list in farhiya_gifts_2:
#     print(list_list)
#     for iten in list_list:
#        result_s = result_s + item 

# print(result_s.strip())

# ## Iterating over a Dictonary

# person_2 = {
#     "name" : "Justin Timberlake",
#     "Age"  : 34,
#     "Skills" : "Other"
# }

# for key in person_2:
#     print(key)
#     # how do I get the values?
#     print(person_2[key])

# # To print out just the dictonary keys
# print(person_2.keys())

# While loops
# A loop with a condition
# Don't make endless loops! Like dividing by 0

# Syntax - break condition at top
# while <condition>:
    # block of code
    # block of code

# Syntax - break condition in body with break
# while True:
    # block of code
    # block of code
    # <condition:
      # break

# counter = 0
# while counter < 10:
#     print("I'm in the while loop")
#     print(counter)
#     counter += 1

# counter = 0
# while True:
#     print("I'm in a while loop")
#     if counter > 10:
#         break
#     counter += 1

## Create small switch boards - good for small games/apps

while True:
    print("Welcome to the Matrix")
    print("Press 1 for hello")
    print('press 2 for a surprise')
    print("press 3 for goodbye")
    user_input = int(input("Enter an option").strip())

    # use user input to control flow what 
    if user_input == 1:
        print("Howdy partner")
    elif user_input == 2:
        print("💩")
    elif user_input == 3:
        print("goodbye")
    else: 
        print("You must choose between 1-3")