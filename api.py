import json ## - Using json to format certain information we have curled.

file = open('cat-api.txt')
content = file.read()

print(f"I read the following information: {content}")

parsed_content =json.loads(content)  # - parse means to decode/interperate it.
print("The parsed content is:")
print(parsed_content)

print(f" deleted status of the third item: {parsed_content[3]}")

