# Python 101

Python is a programming language taht is syntax light and easy to pick up. It is very versatile and well paid. used across the industry for:

- simple scripts 
- Automation (ansibles)
- Web Development ()
- Data and ML
- Others

List of things to cover:

- Data types
- Strings
- Numericals
- Booleans
- List 
- Dictionaties
- IF conditions
- Loops
- While loops
- Debuggers
- Functions
- TDD & Unitesting
- Error handling
- External packages
- API's

Other topics to talk:

- Polymorphism
- TDD
- DRY code
- Seperation of concerns
- ETL - Extract transform and load
- Frameworks (difference to actualy languages)

## Python Intro: Basics

Python you can make files and run them, you can run on the command line or you can have a Framework running in python or use an IDE.

To try stuff out suer quick, use the command line.

```bash
Python3
>>>>>
```

Then you can write python.

```python

>>> human = "Mike Page"
>>> print (human)
Mike Page
>>> 

```

## Virtual Enviornment in Python venv

.venv folder is a virtual environment for python, where you can install packages with pip locally rather than globally in your machine.

You need to start a venv environment and activate it using 'source'.

```bash

python3 -m venv .venv    
source .venv/bin/activate

```

This is to avoid package collusion on your machine and contains the pytho packages installed to this folder.

**requirements.txt** is a text file where you can specify a list of packages your code will need. You can then load it / read it using pip. It also locks in versions.

read it with pip or pip3 using this code:

```bash
pip install -r requirements.txt
```

This will install all the packages in requirements.txt