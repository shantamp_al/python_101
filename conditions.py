# IF conditions
# if conditions is part of control flow.

# When a condition becomes 'True' it runs a block of code.
# There is also an option to program for an 'else' situation where no condition has become 'True'.
# The syntax

#if <condition>:
#    <block of code>
#elif <condition>:
#    <block of code>
#else:
#   <block of code>

# Example
# python uses indentation to outline blocks of code.
# meaning after the collon, a block of code starts where indentation starts and ends when indentation ends.

weather = "Windy"
weather = input('what is the weather? ').strip().lower()

# print(f"{weather}!")
# print(weather == "windy")

# Usually you want to treat user input and format for matching


if weather == "sunny":
    print(f'The weather is {weather}')
    print("Take shades")
elif weather == "windy":
    print('Weather is windy')
    print('Fly a kite')
else:
    print('Weather is not nice')
    print('Take a coat')

if True:
    print('it is true')
else:
    print('it will not be printed')

# New example for the else

if False:
    print(' I will not be printed')
else:
    print('printing from the else')

## Matching with in 
# Python has a "in" matcher that allows you to match with less restrictions
# Works in list, numbers and strings

lottery_number = 128
your_numbers = [10, 120, 30, 50, 140 , 128]

print(128 in your_numbers) # True
print(99 in your_numbers)  # False
print(lottery_number in your_numbers) # True

magic_word = "please"

print(magic_word in " where are the cookies?")

user_question = input("> what would you like?")

if magic_word in user_question:
    print('you shall have what you wish')
else:
    print("you did not use the magic word")#






